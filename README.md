# DB Connect

Database connection Test app. The idea would be to connect to a MySql remote database.

The code to connect to the database is adapted from [saildev](https://saildev.wordpress.com/2014/08/02/connecting-to-mysql-database-from-qml-through-c/)

It also includes code from Michele to connect to a sqlite database through c++

On desktop doesn't seem to connect. On mobile complains about 
`QSqlDatabase: available drivers: QSQLITE`

## License

Copyright (C) 2021  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
