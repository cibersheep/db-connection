/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * dbconnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import SqlDatabase 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'dbconnect.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('DB Connect')
        }

        ColumnLayout {
            spacing: units.gu(2)
            anchors {
                margins: units.gu(2)
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }


            TextField {
                id: host
                Layout.alignment: Qt.AlignHCenter
                placeholderText: i18n.tr('Host')
            }

            TextField {
                id: db
                Layout.alignment: Qt.AlignHCenter
                placeholderText: i18n.tr('DB')
            }

            TextField {
                id: user
                Layout.alignment: Qt.AlignHCenter
                placeholderText: i18n.tr('User')
            }

            TextField {
                id: pwd
                Layout.alignment: Qt.AlignHCenter
                placeholderText: i18n.tr('Password')
                inputMethodHints: Qt.ImhHiddenText
            }

            Label {
                id: label
                Layout.alignment: Qt.AlignHCenter
                text: i18n.tr('Press the button below and check the logs!')
            }

            Button {
                Layout.alignment: Qt.AlignHCenter
                text: i18n.tr('Connect')
                onClicked: SqlDatabase.connectToDatabase(host.text, db.text, user.text, pwd.text)
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }

    Connections {
        target: SqlDatabase

        onConnectedChanged: label.text = "CHANGED" + connected
    }
}
