/*
* Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERPROFILE_H
#define PLAYERPROFILE_H

#include <QObject>
#include <QDebug>
#include <QtSql>


class SqlQueryModel : public QObject
{
    Q_OBJECT

public:
    // explicit SqlQueryModel(QObject *parent = 0);
    SqlQueryModel();
    ~SqlQueryModel();

    Q_INVOKABLE QVariantList read(QString queryStr);

private slots:
    void openDB(void);
    void closeDB(void);

private:
    QSqlDatabase m_db;
};


#endif
