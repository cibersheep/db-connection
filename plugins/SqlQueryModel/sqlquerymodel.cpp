/*
* Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sqlquerymodel.h"
#include <QStandardPaths>

SqlQueryModel::SqlQueryModel()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
}

SqlQueryModel::~SqlQueryModel()
{
    closeDB();
}

void SqlQueryModel::openDB()
{
    if (!QFile::exists(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))) {
        QDir dir;
        bool createOk = dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        if (!createOk) {
            qDebug() << "Unable to create DB directory" << QStandardPaths::writableLocation(QStandardPaths::AppDataLocation).append("/moon.sqlite");
            return;
        }
    }
    // m_db = new QSqlDatabase();
    // m_db->setDatabaseName("qrc:/qml/data/moon.sqlite");
    m_db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation).append("/moon.sqlite"));

    if (!m_db.open())
    qDebug() << "Error opening DB: " << m_db.lastError();
}

QVariantList SqlQueryModel::read(QString queryStr)
{
    if (!m_db.isOpen())
        openDB();
    QSqlQuery query(m_db);
    query.prepare(queryStr);
    QJsonArray recordsArray;

    if (!query.exec()) {
        qDebug() << "exec :" << query.lastError();
    } else {
        while(query.next()) {
            QJsonObject recordObject;
            for(int x=0; x < query.record().count(); x++) {
                recordObject.insert( query.record().fieldName(x),
                QJsonValue::fromVariant(query.value(x)) );
            }
            recordsArray.push_back(recordObject);
        }
    }
    // qDebug() << "QUERY:" << recordsArray;
    return recordsArray.toVariantList();
}

void SqlQueryModel::closeDB()
{
    m_db.close();
}
