/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * dbconnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QSqlDatabase>

#include "sqldatabase.h"

SqlDatabase::SqlDatabase() {
    b_connected = false;
}

void SqlDatabase::connectToDatabase(QString host, QString database, QString username, QString password) {
    QSqlDatabase q_sqlDatabase = QSqlDatabase::addDatabase("QMYSQL");
 
    q_sqlDatabase.setHostName ( host );
    q_sqlDatabase.setDatabaseName ( database );
    q_sqlDatabase.setUserName ( username );
    q_sqlDatabase.setPassword ( password );
 
    if ( q_sqlDatabase.open ( )) {
        b_connected = true;
    }
}
/*
const QString &FilesList::connected() const {
    return b_path;
}
*/

