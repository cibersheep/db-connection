/*
 * Copyright (C) 2021  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * dbconnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQLDATABASE_H
#define SQLDATABASE_H

#include <QObject>

class SqlDatabase : public QObject {
    Q_OBJECT
 
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged);
 
public:
    SqlDatabase();
    ~SqlDatabase() = default;
 
    const bool connected() const;
 
public slots:
    void connectToDatabase(QString host, QString database, QString username, QString password);
 
private:
    bool b_connected;
 
signals:
    void connectedChanged();
 
};

#endif
